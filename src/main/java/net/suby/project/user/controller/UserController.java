package net.suby.project.user.controller;

import net.suby.project.user.service.UserService;
import net.suby.project.user.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;

/**
 * Created by myborn on 2016-12-26.
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/get/{id}")
    public void del(@PathVariable String id) {
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("id", id);
        HashMap<String, String> result = userService.findUserId(parameter);
        System.out.println("mybatis result : " + result);

        UserVO resultUserVO = userService.getUser(id);
        System.out.println("jpa result : " + resultUserVO.getUserNm());
    }
}
