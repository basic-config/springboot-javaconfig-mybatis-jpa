package net.suby.project.user.dao.jpa;

import net.suby.project.user.vo.UserVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by myborn on 2016-12-18.
 */
@Repository
public interface UserJpaRepository extends JpaRepository<UserVO, String> {
}
