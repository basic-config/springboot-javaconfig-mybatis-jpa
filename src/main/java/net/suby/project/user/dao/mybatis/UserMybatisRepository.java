package net.suby.project.user.dao.mybatis;

import java.util.HashMap;

public interface UserMybatisRepository {
    public HashMap<String, String> findUserId(HashMap<String, String> parameter);
}
