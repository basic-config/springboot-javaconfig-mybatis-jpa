package net.suby.project.user.service;

import net.suby.project.user.vo.UserVO;

import java.util.HashMap;

/**
 * Created by myborn on 2016-12-26.
 */
public interface UserService {
    public HashMap<String, String> findUserId(HashMap<String, String> parameter);
    public UserVO getUser(String id);
}
