package net.suby.project.user.service.impl;

import net.suby.project.user.dao.jpa.UserJpaRepository;
import net.suby.project.user.dao.mybatis.UserMybatisRepository;
import net.suby.project.user.service.UserService;
import net.suby.project.user.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * Created by myborn on 2016-12-26.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMybatisRepository userMybatisRepository;

    @Autowired
    private UserJpaRepository userJpaRepository;

    @Override
    public HashMap<String, String> findUserId(HashMap<String, String> parameter) {
        return userMybatisRepository.findUserId(parameter);
    }

    @Override
    public UserVO getUser(String id){
        return userJpaRepository.getOne(id);
    }
}
